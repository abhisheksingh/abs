#!/usr/bin/python
import redis
import urlparse
from threading import Thread
from termcolor import colored
from helper import extract_domainname, get_soup, get_company_website


class ParseCompanyDetail(object):
    '''Parser class that parses different Wikipedia pages
       to get company name and company domain name info
       and stores it in a redis hash table
    '''

    def __init__(self):
        '''initialise object with a redis server
           Key company_info will store company name and domain name
           for the company and key fault_links will store the company
           name and official wiki page of the company. These are the
           pages with no company home page info.
        '''
        self.rserver = redis.Redis('localhost')
        self.base_url = "http://en.wikipedia.org/"

    def _multinational_companies_domain(self, url):
        '''stores comany name and company domain of the mnc
           in company_info dictionary
        '''
        soup = get_soup(url)
        if soup is None:
            return
        a_tags = soup.table.findAll('a', href=True)

        if len(a_tags) is not 0:
            for tag in a_tags:
                cname = tag.text.lower()
                if self.rserver.hexists('company_info', cname) or self.rserver.hexists('faulty_links', cname):
                    continue
                cwiki_link = urlparse.urljoin(self.base_url, tag['href'])
                comp_homepage = get_company_website(cwiki_link)
                if comp_homepage is not None:
                    domainname = extract_domainname(comp_homepage)
                    if domainname is not None:
                        self.rserver.hsetnx('company_info', cname, domainname)
                        print cname.ljust(52) + " ----> ".ljust(8) + colored(domainname.ljust(100), 'blue')
                    else:
                        self.rserver.hsetnx('faulty_links', cname, cwiki_link)
                        print cname.ljust(52) + ' : Domain name is none, official wiki link: '+ colored(cwiki_link.ljust(100), 'blue')
                else:
                    print cname.ljust(52) + " : Company hompage not found at " + colored(cwiki_link.ljust(100), 'blue')
                    self.rserver.hsetnx('faulty_links', cname, cwiki_link)
        else:
            print colored("Error: Unable to parse links on webpage : ", 'red') + colored(url, 'blue')

    def _companies_by_country_domain(self, url):
        '''Gets the company list by country and parses it to
            store company name and company domain in company_info dictionary
        '''
        soup = get_soup(url)
        if soup is None:
            return
        div_tags = soup.findAll('div', {'class':"div-col"})
        if len(div_tags) is not 0:
            for tag in div_tags:
                a_tags = tag.findAll('a', href=True)
                if len(a_tags) is not 0:
                    for tag in a_tags:
                        cname = tag.text.lower()
                        if self.rserver.hexists('company_info', cname) or self.rserver.hexists('faulty_links', cname):
                            continue
                        cwiki_link = urlparse.urljoin(self.base_url, tag['href'])
                        comp_homepage = get_company_website(cwiki_link)
                        if comp_homepage is not None:
                            domainname = extract_domainname(comp_homepage)
                            if domainname is not None:
                                self.rserver.hsetnx('company_info', cname, domainname)
                                print cname.ljust(52) + " ----> ".ljust(8) + colored(domainname.ljust(100), 'blue')
                            else:
                                self.rserver.hsetnx('faulty_links', cname, cwiki_link)
                                print cname.ljust(52) + ' : Domain name is none, official wiki link: '+ colored(cwiki_link.ljust(100), 'blue')
                        else:
                            print cname.ljust(52) + " : Company hompage not found at " + colored(cwiki_link.ljust(100), 'blue')
                            self.rserver.hsetnx('faulty_links', cname, cwiki_link)
                else:
                    print colored("Error: Company links not parsed at ", 'red') + colored(url, 'blue')
        else:
            print colored("Error: Unable to parse links on webpage : ", 'red') + colored(url, 'blue')

    def start_parsing(self, mnc_url, country_url_list):
        '''starts parsing the wikipedia links
        '''
        thread_list = []

        for url in country_url_list:
            th = Thread(target=self._companies_by_country_domain, args=(url,))
            thread_list.append(th)
            th.start()

        t1 = Thread(target=self._multinational_companies_domain, args=(mnc_url,))
        thread_list.append(t1)
        t1.start()

        for thread in thread_list:
            thread.join()

        self.rserver.shutdown()


url_usa ="http://en.wikipedia.org/wiki/List_of_companies_of_the_United_States"
url_india = "http://en.wikipedia.org/wiki/List_of_companies_of_India#XS"
mnc_url = "http://en.wikipedia.org/wiki/List_of_multinational_corporations"

country_url_list = [url_usa, url_india]

if __name__=='__main__':
    cd = ParseCompanyDetail()

    print "================================parsing started====================================="

    cd.start_parsing(mnc_url, country_url_list)

    print "================================parsing finished===================================="
    print
