#!/usr/bin/python
# This module calculates the efficiency of domain finding through
# Google search method. Domain received from the above method is
# matched with the known domain present in the hash named company_info
import redis
from string import join
from termcolor import colored


rserver = redis.Redis('localhost')

gkeys = rserver.hkeys('GoogleResultFound')

match = 0
mismatch = 0

for key in gkeys:
    gdomain = rserver.hget('GoogleResultFound', key)
    wdomain = rserver.hget('company_info', key)

    gdomain = gdomain.split('.')
    wdomain = wdomain.split('.')
    gd = gdomain[0].split('-')
    gd1= gdomain[1].split('-')

    if gdomain[0] in wdomain[0] or wdomain[0] in gdomain[0] or gd[0] in wdomain[0] or wdomain[0] in gdomain[0]:
        match += 1
        print colored("[ OK  ] ".ljust(2), 'green') + key.ljust(53) + " # " + join(gdomain, '.').ljust(35) + " # " + join(wdomain, '.').ljust(25)
    elif len(gdomain) > 2 and gdomain[1] in wdomain[0] or wdomain[0] in gdomain[1] or gd1[0] in wdomain[0] or wdomain[0] in gd1[0]:
        match += 1
        print colored("[ OK  ] ".ljust(2), 'green') + key.ljust(53) + " # " + join(gdomain, '.').ljust(35) + " # " + join(wdomain, '.').ljust(25)
    else:
        mismatch += 1
        print colored("[FAIL ] ", 'red') + key.ljust(53) + " # " + join(gdomain, '.').ljust(35) + " # " + join(wdomain, '.').ljust(25)

fail_count = rserver.hlen('GoogleResultNotFound')

#rserver.shutdown()

efficiency = float(match*100)/(match+mismatch+fail_count)
print
print "Total match : " + str(match)
print "Total mismatch: " + str(mismatch)
print "Efficiency: " + str(efficiency) + "%"
