# abs
Tool that spits out a domain for a company name with a high degree of accuracy.


Dependencies
-------------
* BeautifulSoup
* termcolor
* redis-py
* redis
* google

Steps to install dependencies
------------------------------
$ sudo pip install beatifulsoup4

$ sudo pip install termcolor

$ sudo pip install google

$ sudo pip install redis

Refer <a href="http://redis.io/topics/quickstart"> http://redis.io/topics/quickstart</a> to install redis

Operation instruction
---------------------

* Run wikiparser to get company domain info by parsing Wikipedia pages
  
  make sure redis server is running in other terminal

  $ chmod 755 wikiparser.py

  $ ./wikiparser.py

* Run googleapi to get company domain by google search method

  make sure redis server is running in other terminal

  $ chmod 755 googleapi.py

  $ ./googleapi.py

* Run compare module to check the efficiency of google result

   make sure redis server is running in other terminal

   $ chmod 755 compare.py
 
   $ ./compare.py

